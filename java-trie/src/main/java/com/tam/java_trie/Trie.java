package com.tam.java_trie;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * This Trie class is behaving as the root node because in my implementation you
 * can't have a Node with no key.
 *
 */
public class Trie {

	private ArrayList<Node> children;
	private int id; // id for nodes for clarity when printing

	public Trie() throws Exception {
		children = new ArrayList<Node>();
		id = 1;
	}

	/**
	 * If the word is already in the trie, this ensures the last letter has a flag
	 * indicating that it's a valid word. If the word isn't in the trie, adds the
	 * remaining letters/nodes to the end of the relevant node.
	 * 
	 * @param word
	 * @throws Exception
	 */
	public void addWord(String word) throws Exception {
		char[] characters = word.trim().toCharArray();

		// Don't want empty words
		if (characters.length == 0) {
			return;
		}

		Node insertAtNode = null;

		// Get started since the root isn't a real node.
		char first = characters[0];
		for (Node node : children) {
			if (node.isKey(first)) {
				insertAtNode = node;
			}
		}

		if (insertAtNode == null) {
			// The word needs to be added at the top level of the trie.
			Boolean isSingleLetterWord = (characters.length == 1);
			Node newTopLevelNode = new Node(first, isSingleLetterWord, id);
			id++;
			this.children.add(newTopLevelNode);
			if (!isSingleLetterWord) {
				insertCharsAtNode(Arrays.copyOfRange(characters, 1, characters.length), newTopLevelNode);
			}
			return;
		}

		Boolean inserted = addWord(Arrays.copyOfRange(characters, 0, characters.length), insertAtNode);
		if (!inserted) {
			throw new Exception("Failed to insert word " + word);
		}
	}

	/**
	 * Recurses down a trie of nodes to find the right place to insert a new word or
	 * word ending.
	 * 
	 * @param word
	 * @param node
	 * @throws Exception
	 * @return whether or not the word was successfully inserted.
	 */
	private Boolean addWord(char[] word, Node node) throws Exception {
		Boolean lastLetter = (word.length == 1);
		char charToFind = word[0];

		if (node.isKey(charToFind)) {
			if (lastLetter) {
				// The word was already in the trie, just need to
				// make sure it's marked as a valid word.
				node.setEndOfWord(true);
				return true;
			} else {
				for (Node child : node.getChildren()) {
					Boolean addedToChild = addWord(Arrays.copyOfRange(word, 1, word.length), child);
					if (addedToChild) {
						return true;
					}
				}
				// If we get to this point, the current node matches a letter mid way
				// through the word and none of the child nodes are the next letter.
				// So add the rest of the letters here.
				insertCharsAtNode(Arrays.copyOfRange(word, 1, word.length), node);
				return true;
			}
		} else {
			// This is a branch of the trie which doesn't match our word.
			return false;
		}
	}

	public Boolean validWord(String word) {
		char[] characters = word.trim().toCharArray();

		// Don't want empty words
		if (characters.length == 0) {
			return false;
		}

		Node startAtNode = null;

		// Get started since the root isn't a real node.
		char first = characters[0];
		for (Node node : children) {
			if (node.isKey(first)) {
				if (characters.length == 1) {
					// One letter word. Found it.
					return true;
				}
				startAtNode = node;
			}
		}
		if (startAtNode == null) {
			// This indicates the first character of the word wasn't one of the root
			// characters of the trie.
			return false;
		}
		
		return validWord(Arrays.copyOfRange(characters, 0, characters.length), startAtNode);
	}
	
	/**
	 * Recurses down a trie of nodes to find whether the word is valid.
	 * 
	 * @param word
	 * @param node
	 * @return whether or not the word was valid
	 */
	private Boolean validWord(char[] word, Node node) {
		Boolean lastLetter = (word.length == 1);
		char charToFind = word[0];

		if (node.isKey(charToFind)) {
			if (lastLetter) {
				// The word was found, just need to check if it's a valid end of word.
				return node.isEndOfWord();
			} else {
				for (Node child : node.getChildren()) {
					Boolean foundInChild = validWord(Arrays.copyOfRange(word, 1, word.length), child);
					if (foundInChild) {
						return true;
					}
				}
				// If we get to this point, the current node matches a letter mid way
				// through the word and none of the child nodes are the next letter.
				// So the word was not found.
				return false;
			}
		} else {
			// This is a branch of the trie which doesn't match our word.
			return false;
		}
	}

	/**
	 * Creates new Nodes for the chars under the given node.
	 * 
	 * @param chars
	 * @param node
	 */
	private void insertCharsAtNode(char[] chars, Node node) throws Exception {
		Node newNode = null;
		for (char c : chars) {
			newNode = new Node(c, false, id);
			id++;
			node.addChild(newNode);
			node = newNode;
		}
		// The last one is the end of the word.
		node.setEndOfWord(true);
	}
	
	/**
	 * If the word is in the trie: - if the word is in the middle of a bigger word,
	 * removes the flag from the last letter indicating that it was a valid word -
	 * otherwise, deletes the leaf nodes up to the next valid word If the word isn't
	 * in the trie, silently does nothing.
	 * 
	 * @param word
	 */
	public void deleteWord(String word) {
		// char[] characters = word.trim().toCharArray();
		throw new UnsupportedOperationException();
	}

	public void printTrie() {
		for (Node node : children) {
			printNode(node, "");
		}
	}

	private void printNode(Node node, String parents) {
		String prettyNode = "(" + node.getKey() + ", " + node.isEndOfWord() + ", " + node.getId() + ")";
		parents += " > " + prettyNode;

		if (node.getChildren().isEmpty()) {
			System.out.println(parents);
		}

		for (Node child : node.getChildren()) {
			printNode(child, parents);
		}
	}

}

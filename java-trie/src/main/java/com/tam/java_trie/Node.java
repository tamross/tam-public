package com.tam.java_trie;

import java.util.ArrayList;

/**
 * A trie is made up of nodes. 
 * A node consists of:
 *   - a single letter;
 *   - a list of it's child nodes - all the possible next letters after this one in the trie;
 *   - whether it's the last node on a branchl; and
 *   - whether the letter can be a valid end of a word.
 * 
 * I've also given nodes an ID for the sole purpose of being able to see what's what when
 * printing the trie.
 *
 */
public class Node {
	
	private char key;
	private Boolean endOfWord;
	private ArrayList<Node> children = new ArrayList<Node>();
	private int id;
	
	public Node(char key, Boolean endOfWord, int id) throws Exception {
		this.endOfWord = endOfWord;
		this.id = id;
		// let's have all keys in lowercase
		if (!Character.isAlphabetic(key)) {
			throw new Exception("Key should be a letter, not " + key);
		}
		this.key = Character.toLowerCase(key);
	}

	public char getKey() {
		return key;
	}
	
	public Boolean isKey(char c) {
		return c == this.key;
	}

	public Boolean isEndOfWord() {
		return endOfWord;
	}

	public void setEndOfWord(Boolean endOfWord) {
		this.endOfWord = endOfWord;
	}

	public ArrayList<Node> getChildren() {
		return children;
	}
	
	public Boolean hasChildren() {
		return !children.isEmpty();
	}
	
	public void addChild(Node child) {
		children.add(child);
	}

	public int getId() {
		return id;
	}
}

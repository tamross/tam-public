package com.tam.java_trie;

import junit.framework.TestCase;

public class NodeTest extends TestCase {
	
	public void testValidCharacters()  {
		Node node;
		try {
			node = new Node('a', true, 1);
			assertEquals(node.getKey(), 'a');
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	public void testUppercaseCharacters() {
		Node node;
		try {
			node = new Node('A', true, 1);
			assertEquals(node.getKey(), 'a');
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	public void testNumbers() {
		Boolean exceptionThrown = false;
		try {
			@SuppressWarnings("unused")
			Node node = new Node('1', true, 1);
		} catch (Exception e) {
			exceptionThrown = true;
		}
		assertTrue(exceptionThrown);
	}
	
	public void testChildred() throws Exception {
		Node nodeT = new Node('t', false, 1);
		Node nodeO = new Node('o', true, 2);
		nodeT.addChild(nodeO);
		assertTrue(nodeT.hasChildren());
	}

}

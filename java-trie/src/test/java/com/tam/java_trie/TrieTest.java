package com.tam.java_trie;

import junit.framework.TestCase;

public class TrieTest extends TestCase {
	
	Trie trie;
	
	@Override
	protected void setUp() throws Exception {
		trie = new Trie();
		trie.addWord("a");
		trie.addWord("an");
		trie.addWord("and");
		trie.addWord("any");
		trie.addWord("anything");
		trie.addWord("bar");
		trie.addWord("barked");
		trie.addWord("banished");
		trie.addWord("carpet");
		trie.addWord("car");
		trie.addWord("cart");
		trie.printTrie();
		System.out.println("--------------");
	}
	
	public void testValidWords() {
		assertTrue(trie.validWord("a"));
		assertTrue(trie.validWord("an"));
		assertTrue(trie.validWord("and"));
		assertTrue(trie.validWord("any"));
		assertTrue(trie.validWord("anything"));
		assertTrue(trie.validWord("bar"));
		assertTrue(trie.validWord("barked"));
		assertTrue(trie.validWord("banished"));
		assertTrue(trie.validWord("car"));
		assertTrue(trie.validWord("carpet"));
		assertTrue(trie.validWord("cart"));
	}
	
	public void testInvalidWords() {		
		assertFalse(trie.validWord(""));
		assertFalse(trie.validWord("anyt"));
		assertFalse(trie.validWord("ba"));
		assertFalse(trie.validWord("barke"));
		assertFalse(trie.validWord("banishe"));
		assertFalse(trie.validWord("ar"));
		assertFalse(trie.validWord("d"));
		assertFalse(trie.validWord("ything"));
	}
	
	public void testDeleteWord() {
		assertTrue(trie.validWord("any"));
		trie.deleteWord("any");
		assertFalse(trie.validWord("any"));
		
		assertTrue(trie.validWord("anything"));
		trie.deleteWord("anything");
		assertFalse(trie.validWord("anything"));
	}

}
